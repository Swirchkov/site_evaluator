﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ResponseMeasurer
{
    public class ResponseMeasurer
    {
        public long testUrl(string url)
        {
            Stopwatch sw = new Stopwatch();
            using (HttpClient client = new HttpClient())
            {
                sw.Start();
                HttpResponseMessage response = client.GetAsync(url).Result;
                sw.Stop();
            }
            return sw.ElapsedMilliseconds;
        }
    }
}
