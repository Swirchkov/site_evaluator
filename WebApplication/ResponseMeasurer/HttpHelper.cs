﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ResponseMeasurer
{
    internal class HttpHelper
    {
        internal string getFileContent(string url)
        {
            HttpResponseMessage response = null;

            using (HttpClient client = new HttpClient())
            {
                response = client.GetAsync(url).Result;
            }

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
