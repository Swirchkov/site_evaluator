﻿var app = angular.module('EvaluateApp');

app.controller('searchController', function ($scope, hubService) {
    
    $scope.url = null;

    // url list with pagination
    $scope.urlEnumerable = null;
    $scope.currentUrls = [];
    $scope.urlsPerView = 50;
    $scope.ranges = [];
    $scope.selectedRange = 0;

    //tests
    $scope.tests = new TestsCollection();
    $scope.INIT_MEASURES = 3;

    function checkUrl(url) {
        if (url == null) { return false; }

        url = url.trim();

        if (url == '') { return false; }

        var pattern = /((https?:)\/\/)?((\w+\.)+\w+)(:(\d)+)?((\/\w+)+)?/i;
        return url.match(pattern) != null;
    };
    $scope.getNumbers = function (number) {
        return Array.range(0, number);
    }

    $scope.searchUrls = function () {

        if (!checkUrl($scope.url)) { console.error('bad url'); return; }

        hubService.processUrl($scope.url).then(function (data) {

            $scope.urlEnumerable = new Enumerable(data);

            var count = $scope.urlEnumerable.countRanges($scope.urlsPerView);
            $scope.ranges = Array.apply(null, Array(count)).map(function (elem, index) { return index });

            $scope.selectRange(0);

            setTimeout(function () {
                $scope.$apply();
            }, 1);
        });
    };

    $scope.selectRange = function (number) {
        if ($scope.urlEnumerable.countRanges($scope.urlsPerView) > number) {
            $scope.selectedRange = number;
            $scope.currentUrls = $scope.urlEnumerable.getRange(number, $scope.urlsPerView);

            $scope.initMeasures($scope.INIT_MEASURES);

            setTimeout(function () {
                $scope.$apply();
            }, 1);
        }
    };

    $scope.initMeasures = function (number) {
        for (var i = 0; i < number; i++) {
            $scope.performMeasure();
        }
        console.log($scope.tests.countMeasures());
    };

    $scope.performMeasure = function () {
        $scope.currentUrls.forEach(function (url) {
            $scope.testUrl(url);
        });
    };

    $scope.testUrl = function (url) {
        hubService.testUrl(url).then(function (response) {
            $scope.tests.addUrlMeasure(url, response);

            setTimeout(function () {
                $scope.$apply();
            }, 1);
        });
    };

});