﻿Array.range = function (start, count) {
    var arr = [];

    for (var i = 0 ; i < count; i++) {
        arr.push(start + i);
    }

    return arr;
}