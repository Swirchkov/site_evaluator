﻿function TestsCollection() {
    var dictionary = {};

    var urls = [];

    this.addUrlMeasure = function (url, measure) {

        if (urls.indexOf(url) != -1) {
            dictionary[url].push(measure);
        }
        else {
            urls.push(url);
            dictionary[url] = [measure];
        }

    };

    this.countMeasures = function () {

        if (urls.length == 0) { return 0; }

        var max = 1;
        urls.forEach(function (value) {
            if (dictionary[value].length > max) {
                max = dictionary[value].length;
            }
        });

        return max;
    };

    this.getUrlMeasures = function (url) {
        return dictionary[url];
    };

    this.DICT = dictionary;
};