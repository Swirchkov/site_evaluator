﻿var Enumerable = function (elements) {

    if (typeof (elements) != typeof ([])) {
        throw new Error('Invalid elements type. Expected array but get - "' + typeof(elements) + '". ');
    }

    this.countRanges = function (elementsPerRequest) {
        return Math.ceil(elements.length * 1.0 / elementsPerRequest);
    };

    this.getRange = function (rangeNumber, elementsPerRequest) {

        if (rangeNumber < 0 || rangeNumber > this.countRanges(elementsPerRequest)) {
            return [];
        }

        if ((rangeNumber + 1) * elementsPerRequest < elements.length) {
            return elements.slice(rangeNumber * elementsPerRequest, (rangeNumber + 1) * elementsPerRequest);
        }
        else {
            return elements.slice(rangeNumber * elementsPerRequest, elements.length);
        }
    }
}