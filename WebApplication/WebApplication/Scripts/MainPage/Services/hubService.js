﻿var app = angular.module('EvaluateApp');

app.service('hubService', function () {

    var evaluateHub = $.connection.evaluateHub;
    console.log(evaluateHub);

    this.processUrl = function (url) {
        console.log('calling server hub');
        var urls = evaluateHub.server.getUrls(url);
        return urls;
    };

    this.testUrl = function (url) {
        return evaluateHub.server.testUrl(url);
    };

    $.connection.hub.start();
});