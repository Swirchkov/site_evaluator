﻿using Microsoft.AspNet.SignalR;
using System.Text.RegularExpressions;
using SitemapLib;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace WebApplication.Hubs
{
    public class EvaluateHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }
        public IEnumerable<string> getUrls(string rootUrl)
        {
            // validate url
            if (!validateUrl(rootUrl)) { return new string[] { }; }

            Sitemap map = new Sitemap(rootUrl);
            return map.GetUrlsFromSitemap();
        }
        private bool validateUrl(string url)
        {
            if (url == null) return false;

            url = url.Trim();

            if (url == "") return false;

            string pattern = @"((https?:)\/\/)?((\w+\.)+\w+)(:(\d)+)?((\/\w+)+)?";
            Regex regex = new Regex(pattern);

            return regex.IsMatch(url);
        }
        public long testUrl(string url)
        {
            if (!validateUrl(url)) { return -1; }

            ResponseMeasurer.ResponseMeasurer rm = new ResponseMeasurer.ResponseMeasurer();

            return rm.testUrl(url);
        }
    }
}