﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SitemapLib
{
    public class Sitemap
    {
        private string url;
        private readonly short URL_LIMIT = 1000;
        public Sitemap(string url)
        {
            this.url = url;
        }

        public IEnumerable<string> GetUrlsFromSitemap()
        {
            IEnumerable<string> sitemaps = this.determineSitemapPath();

            List<string> urls = new List<string>() { this.url };
            HttpHelper helper = new HttpHelper();

            foreach (string sitemapUrl in sitemaps)
            {
                // seperate task for each sitemap file
                string content = helper.getFileContent(sitemapUrl);
                urls.AddRange(this.parseSitemapFile(content));

                if (urls.Count >= this.URL_LIMIT) break;
                
            }

            if (urls.Count > this.URL_LIMIT)
            {
                return urls.GetRange(0, this.URL_LIMIT - 1).Distinct();
            }
            else
            {
                return urls.Distinct();
            }
        }

        public IEnumerable<string> parseSitemapFile(string content)
        {
            XDocument doc = XDocument.Parse(content);

            if (doc.Root.Name.LocalName == "sitemapindex")
            {
                return this.parseCollectionSitemap(doc);
            }
            else
            {
                return this.parseSitemapContent(doc);
            }
        }

        private IEnumerable<string> parseCollectionSitemap(XDocument doc)
        {
            // we select url's from sitemap files till we reach number of 1000 urls
            List<string> urls = new List<string>();
            HttpHelper helper = new HttpHelper();

            foreach (XElement sitemap in doc.Root.Elements())
            {
                if (urls.Count > this.URL_LIMIT)
                {
                    break;
                }

                string location = sitemap.Elements().First(elem => elem.Name.LocalName == "loc").Value;
                string content = helper.getFileContent(location);

                IEnumerable<string> fileUrls = null;

                if (location.EndsWith("xml"))
                {
                    fileUrls = this.parseSitemapContent(XDocument.Parse(content));
                }
                else if (location.EndsWith("txt"))
                {
                    TxtSitemapWorker txtWorker = new TxtSitemapWorker();
                    fileUrls = txtWorker.parseTextContent(content);
                }

                urls.AddRange(fileUrls);
                urls = new List<string>(urls.Distinct());
            }

            return urls;
        }

        private IEnumerable<string> parseSitemapContent(XDocument doc)
        {
            List<string> urls = new List<string>();

            foreach (XElement urlElement in doc.Root.Elements())
            {
                string url = urlElement.Elements().First(elem => elem.Name.LocalName == "loc").Value;
                urls.Add(url);
            }

            return urls;
        }

        private IEnumerable<string> determineSitemapPath()
        {
            HttpHelper helper = new HttpHelper();
            string path = "/sitemap.xml";

            if (helper.isUrlAvailable(this.url + path))
            {
                return new string[] { this.url + path };
            }

            path = "/robots.txt";
            if (helper.isUrlAvailable(this.url + path))
            {
                string content = helper.getFileContent(this.url + path);

                Robots robots = new Robots(content);
                return robots.FindSitemapLinks();
            }

            return new string[] { };
        }
    }
}
