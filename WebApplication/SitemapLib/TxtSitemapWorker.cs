﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SitemapLib
{
    internal class TxtSitemapWorker
    {
        internal TxtSitemapWorker() { }

        internal IEnumerable<string> parseTextContent(string content)
        {
            // we know allmost nothing about file format, so just search urls using regular expression
            Regex exp = new Regex(@"((https?:)\/\/)?((\w+\.)+\w+)(:(\d)+)?((\/\w+)+)?");

            List<string> urls = new List<string>();
            Match match = exp.Match(content);

            do
            {
                urls.Add(match.Value);
                match = match.NextMatch();
            } while (match.Value != "");

            return urls;
        }
    }
}
