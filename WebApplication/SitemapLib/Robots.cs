﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SitemapLib
{
    internal class Robots
    {
        private string content = null;
        internal Robots(string content)
        {
            this.content = content;
        }

        internal IEnumerable<string> FindSitemapLinks()
        {
            IEnumerable<string> lines = content.Split('\n');

            lines = lines.Where(line => line.StartsWith("Sitemap"));

            return lines.Select(line => line.Split(' ').Last()).Distinct();
        }
    }
}
