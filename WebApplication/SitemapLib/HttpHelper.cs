﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SitemapLib
{
    internal class HttpHelper
    {
        internal HttpHelper() { }

        internal bool isUrlAvailable(string url)
        {
            HttpResponseMessage response = null;

            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url);
                response = client.SendAsync(request).Result;
            }

            return response.IsSuccessStatusCode;
        }

        internal string getFileContent(string url)
        {
            HttpResponseMessage response = null;

            using (HttpClient client = new HttpClient())
            {
                response = client.GetAsync(url).Result;
            }

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
